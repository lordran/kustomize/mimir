#!/bin/sh

work_dir=$(dirname $0)

echo 'AWS_ACCESS_KEY_ID='$(pass perso/Servers/Wasabi/Mimir/s3_access_key) >$work_dir/secrets.env
echo 'AWS_SECRET_ACCESS_KEY='$(pass perso/Servers/Wasabi/Mimir/s3_secret_key) >>$work_dir/secrets.env
